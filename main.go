package main

import (
	"fmt"
	"log"
	"os"
	"sort"

	"github.com/asdine/storm"
	"gitlab.com/sacules/melilurk/meli"
)

const (
	dbName = `products.db`
)

// only the info i'm interested in from the api
type item struct {
	ID    string `storm:"id,unique"`
	Title string
	Price float64 `storm:"index"`
}

func fetch(db *storm.DB, category string) []item {
	var items []item
	log.Println("fetching from category", category)

	total := 9999 // dummy until first req
	for i := 0; i < total; i += 50 {
		url := meli.BuildURL(category, i)
		res, err := meli.Search(url)
		if err != nil {
			log.Fatal(err)
		}

		total = res.Paging.Total

		for _, r := range res.Results {
			it := item{r.ID, r.Title, r.Price}
			err = db.One("ID", r.ID, &it)
			switch err {
			case nil:
				continue

			case storm.ErrNotFound:
				items = append(items, it)
				err = db.Save(&it)
				if err != nil {
					log.Println("saving:", err)
				}

			default:
				log.Println("saving:", err)
			}
		}
	}

	return items
}

func main() {
	db, err := storm.Open(dbName)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if len(os.Args) > 1 && os.Args[1] == "sort" {
		var items []item
		err = db.All(&items)
		if err != nil {
			log.Fatal(err)
		}

		sort.Slice(items, func(i, j int) bool {
			return items[i].Price < items[j].Price
		})

		for i, it := range items {
			fmt.Printf("%d. %s | $%.2f (ID %s)\n", i, it.Title, it.Price, it.ID)
		}

		return
	}

	items := fetch(db, meli.CategoryLabo)
	items = append(items, fetch(db, meli.CategoryRollos)...)

	if len(items) == 0 {
		log.Println("nothing new")
		return
	}

	sort.Slice(items, func(i, j int) bool {
		return items[i].Price < items[j].Price
	})
	for i, it := range items {
		fmt.Printf("%d. %s | $%.2f (ID %s)\n", i, it.Title, it.Price, it.ID)
	}
}
