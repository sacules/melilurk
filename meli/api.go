package meli

const (
	CategoryRollos = `MLA4238`
	CategoryLabo   = `MLA10372`
)

type SearchResponse struct {
	AvailableFilters []struct {
		ID     string `json:"id"`
		Name   string `json:"name"`
		Type   string `json:"type"`
		Values []struct {
			ID      string `json:"id"`
			Name    string `json:"name"`
			Results int    `json:"results"`
		} `json:"values"`
	} `json:"available_filters"`
	AvailableSorts []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"available_sorts"`
	Filters []struct {
		ID     string `json:"id"`
		Name   string `json:"name"`
		Type   string `json:"type"`
		Values []struct {
			ID           string `json:"id"`
			Name         string `json:"name"`
			PathFromRoot []struct {
				ID   string `json:"id"`
				Name string `json:"name"`
			} `json:"path_from_root"`
		} `json:"values"`
	} `json:"filters"`
	Paging struct {
		Limit          int `json:"limit"`
		Offset         int `json:"offset"`
		PrimaryResults int `json:"primary_results"`
		Total          int `json:"total"`
	} `json:"paging"`
	RelatedResults []interface{} `json:"related_results"`
	Results        []struct {
		AcceptsMercadopago bool `json:"accepts_mercadopago"`
		Address            struct {
			CityID    string `json:"city_id"`
			CityName  string `json:"city_name"`
			StateID   string `json:"state_id"`
			StateName string `json:"state_name"`
		} `json:"address"`
		Attributes []struct {
			AttributeGroupID   string      `json:"attribute_group_id"`
			AttributeGroupName string      `json:"attribute_group_name"`
			ID                 string      `json:"id"`
			Name               string      `json:"name"`
			Source             int         `json:"source"`
			ValueID            string      `json:"value_id"`
			ValueName          string      `json:"value_name"`
			ValueStruct        interface{} `json:"value_struct"`
			Values             []struct {
				ID     string      `json:"id"`
				Name   string      `json:"name"`
				Source int         `json:"source"`
				Struct interface{} `json:"struct"`
			} `json:"values"`
		} `json:"attributes"`
		AvailableQuantity   int    `json:"available_quantity"`
		BuyingMode          string `json:"buying_mode"`
		CatalogProductID    string `json:"catalog_product_id"`
		CategoryID          string `json:"category_id"`
		Condition           string `json:"condition"`
		CurrencyID          string `json:"currency_id"`
		DifferentialPricing struct {
			ID int `json:"id"`
		} `json:"differential_pricing"`
		ID           string `json:"id"`
		Installments struct {
			Amount     float64 `json:"amount"`
			CurrencyID string  `json:"currency_id"`
			Quantity   int     `json:"quantity"`
			Rate       float64 `json:"rate"`
		} `json:"installments"`
		ListingTypeID   string      `json:"listing_type_id"`
		OfficialStoreID int         `json:"official_store_id"`
		OriginalPrice   interface{} `json:"original_price"`
		Permalink       string      `json:"permalink"`
		Price           float64     `json:"price"`
		Seller          struct {
			CarDealer bool `json:"car_dealer"`
			Eshop     struct {
				EshopExperience int           `json:"eshop_experience"`
				EshopID         int           `json:"eshop_id"`
				EshopLocations  []interface{} `json:"eshop_locations"`
				EshopLogoURL    string        `json:"eshop_logo_url"`
				EshopRubro      interface{}   `json:"eshop_rubro"`
				EshopStatusID   int           `json:"eshop_status_id"`
				NickName        string        `json:"nick_name"`
				Seller          int           `json:"seller"`
				SiteID          string        `json:"site_id"`
			} `json:"eshop"`
			ID                int           `json:"id"`
			Permalink         string        `json:"permalink"`
			PowerSellerStatus string        `json:"power_seller_status"`
			RealEstateAgency  bool          `json:"real_estate_agency"`
			Tags              []interface{} `json:"tags"`
		} `json:"seller"`
		SellerAddress struct {
			AddressLine string `json:"address_line"`
			City        struct {
				ID   string `json:"id"`
				Name string `json:"name"`
			} `json:"city"`
			Comment string `json:"comment"`
			Country struct {
				ID   string `json:"id"`
				Name string `json:"name"`
			} `json:"country"`
			ID        string `json:"id"`
			Latitude  string `json:"latitude"`
			Longitude string `json:"longitude"`
			State     struct {
				ID   string `json:"id"`
				Name string `json:"name"`
			} `json:"state"`
			ZipCode string `json:"zip_code"`
		} `json:"seller_address"`
		Shipping struct {
			FreeShipping bool     `json:"free_shipping"`
			LogisticType string   `json:"logistic_type"`
			Mode         string   `json:"mode"`
			StorePickUp  bool     `json:"store_pick_up"`
			Tags         []string `json:"tags"`
		} `json:"shipping"`
		SiteID       string   `json:"site_id"`
		SoldQuantity int      `json:"sold_quantity"`
		StopTime     string   `json:"stop_time"`
		Tags         []string `json:"tags"`
		Thumbnail    string   `json:"thumbnail"`
		Title        string   `json:"title"`
	} `json:"results"`
	SecondaryResults []interface{} `json:"secondary_results"`
	SiteID           string        `json:"site_id"`
	Sort             struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"sort"`
}
