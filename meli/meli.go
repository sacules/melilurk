package meli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func BuildURL(category string, offset int) string {
	return fmt.Sprintf("https://api.mercadolibre.com/sites/MLA/search?category=%s&offset=%d",
		category, offset)
}

func Search(url string) (*SearchResponse, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res SearchResponse
	err = json.Unmarshal(body, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
