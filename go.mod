module gitlab.com/sacules/melilurk

go 1.14

require (
	github.com/DataDog/zstd v1.4.5 // indirect
	github.com/Sereal/Sereal v0.0.0-20200430150152-3c99d16fbeb1 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	go.etcd.io/bbolt v1.3.5 // indirect
	golang.org/x/sys v0.0.0-20201020230747-6e5568b54d1a // indirect
	google.golang.org/appengine v1.6.6 // indirect
)
